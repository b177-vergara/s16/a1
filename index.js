let num = prompt("Input a number");
console.log("The number you provided is " + num);

for(let i = num; i > 0; i--){

	if(i <= 50 ){
		console.log("The current value is less than or equal to 50. Terminating the loop.");
		break;
	}

	if(i % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number");
	}

	if(i % 5 == 0 && i % 10 != 0){
		console.log(i);
	}

}

let initial = "supercalifragilisticexpialidocious";
let final ="";

console.log(initial);

for(let x = 0; x < initial.length; x++){

	if(
		initial[x].toLowerCase() === "a" ||
		initial[x].toLowerCase() === "e" ||
		initial[x].toLowerCase() === "i" ||
		initial[x].toLowerCase() === "o" ||
		initial[x].toLowerCase() === "u"
	){
		continue;
	}

	else{
		final +=initial[x];
	}
}

console.log(final);